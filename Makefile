
all: build

build:
	mkdir -p build
	$(MAKE) --directory=$(shell pwd)/code

run:
	./test-ops

test:
	./tests
