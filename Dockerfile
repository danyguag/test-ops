FROM fedora:latest AS build
LABEL maintainer="danyguag2@gmail.com"
RUN dnf update -y
RUN yum -y install zsh gcc make

RUN adduser builder
USER builder
ADD --chown=builder:builder ./ /home/builder/app
WORKDIR /home/builder/app
RUN make

FROM fedora:latest
RUN dnf install -y zsh make
RUN adduser user
USER user
COPY --from=build --chown=user:user /home/builder/app /home/user/app
WORKDIR /home/user/app
